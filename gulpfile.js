var gulp = require('gulp');
var stylus = require('gulp-stylus');
var pug = require('gulp-pug')
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

gulp.task('stylus', function () {
  return gulp.src('./app/styles/style.styl')
    .pipe(stylus())
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/styles'))
    .pipe(reload({stream:true}));
});

gulp.task('pug', function () {
  return gulp.src('./app/pages/*.pug')
    .pipe(pug( {
		pretty: true
	} ))
    .pipe(gulp.dest('./dist/'))
    .pipe(reload({stream:true}));
});

gulp.task('watcher', function(){
  gulp.watch('./app/styles/style.styl', ['stylus'])
  gulp.watch('./app/blocks/**/*.styl', ['stylus'])
  gulp.watch('./app/pages/*.pug', ['pug'])
  gulp.watch('./app/blocks/**/*.pug', ['pug'])
});
gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: "dist"
    },
    open: false,
    notify: false
  });
});

gulp.task('default', ['watcher', 'browserSync']);
